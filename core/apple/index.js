const Apple = function (opts) {
    this.name = opts.name;
};
Apple.prototype.say = function () {
    console.log(`I'm ${this.name}`);
};
Apple.prototype.store = function () {
    console.log('Stored...');
};
module.exports = Apple;